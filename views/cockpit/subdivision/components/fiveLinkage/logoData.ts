const logoData = [
  {
    value: 1,
    label: '六区一县范围'
  },
  {
    value: 2,
    label: '轨道3号线'
  },
  {
    value: 3,
    label: '轨道7号线'
  },
  {
    value: 4,
    label: '中心城区范围'
  },
  {
    value: 5,
    label: '轨道4号线'
  },
  {
    value: 6,
    label: '轨道8号线'
  },
  {
    value: 7,
    label: '轨道1号线'
  },
  {
    value: 8,
    label: '轨道5号线'
  },
  {
    value: 9,
    label: '轨道9号线'
  },
  {
    value: 10,
    label: '轨道2号线'
  },
  {
    value: 11,
    label: '轨道6号线'
  },
  {
    value: 12,
    label: '轨道10号线'
  },
  {
    value: 13,
    label: '轨道11号线'
  },
  {
    value: 14,
    label: '城际铁路'
  },
  {
    value: 15,
    label: '机场'
  },
  {
    value: 16,
    label: '轨道12号线'
  },
  {
    value: 17,
    label: '普速铁路'
  },
  {
    value: 18,
    label: '高铁站'
  },
  {
    value: 19,
    label: '轨道13号线'
  },
  {
    value: 20,
    label: '都市区快线'
  },
  {
    value: 21,
    label: '城际站'
  },
  {
    value: 22,
    label: '高速铁路'
  },
  {
    value: 23,
    label: '磁浮线'
  },
  {
    value: 24,
    label: '汽车站'
  }
]
export default logoData
