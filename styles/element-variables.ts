export interface IElementScssVariables {
  theme: string
}

export let elementVariables: IElementScssVariables

export default elementVariables
